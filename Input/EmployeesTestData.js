function addExtraChars(limit) {
    var extraChars = Math.floor(Math.random()*limit);
    return extraChars;
}

var EmployeesTestDataList = (function(){
    
    function EmployeesTestDataList(){
        
        this.newEmployee = {
            'name':'Eve Polastri ' + addExtraChars(9999999),
            'salary':'1000',
            'age':'44'
        }   

        this.noName = {
            'salary':'1000',
            'age':'32'
        }   

        this.blankName = {
            'name':'',
            'salary':'2000',
            'age':'44'             
        }

        this.specialCharsName = {
            'name':'Abc!@#$%^&*()<>+=~`{[]}():;/<>\\|' + addExtraChars(9999999),
            'salary':'1001',
            'age':'44'          
        }

        this.maxCharsName = {   
            'name':'Max Chars Test ' + addExtraChars(999999999999999),   
            'salary':'1001',
            'age':'44'          
        }

        this.nonStringName = {
            'name':''+addExtraChars(9999999),
            'salary':'2001',
            'age':'44'  
        }

        this.duplicateName = {
            'name':'Benjamin',
            'salary':'2002',
            'age':'42'
        }

        this.errorMsgNoName = {
            'errorMsg':'{\"error\":{\"text\":SQLSTATE[23000]: Integrity constraint violation: 1048 Column \'employee_name\' cannot be null}}'
        }

        this.errorMsgBlankName = {
            'errorMsg':'{\"error\":{\"text\":SQLSTATE[23000]: Integrity constraint violation: 1062 Duplicate entry \'\' for key \'employee_name_unique\'}}'
        }

        this.errorMsgDuplicateName = {
            'errorMsg':'{\"error\":{\"text\":SQLSTATE[23000]: Integrity constraint violation: 1062 Duplicate entry \'Benjamin\' for key \'employee_name_unique\'}}'
        }

        this.errorMsgNoSalary = {
            'errorMsg':'{\"error\":{\"text\":SQLSTATE[23000]: Integrity constraint violation: 1048 Column \'employee_salary\' cannot be null}}'
        }

        this.errorMsgNoAge = {
            'errorMsg':'{\"error\":{\"text\":SQLSTATE[23000]: Integrity constraint violation: 1048 Column \'employee_age\' cannot be null}}'
        }

        this.errorMsgNonExistentId = {
            'errorMsg':'{\"error\":{\"text\":SQLSTATE[42S22]: Column not found: 1054 Unknown column \'abc123\' in \'where clause\'}}'
        }

        this.negativeSalary = {
            'name':'Villanelle ' + addExtraChars(9999999),
            'salary':'-1',
            'age':'28'
        }

        this.noSalary = {
            'name':'Villanelle ' + addExtraChars(9999999),
            'age':'28'
        }

        this.blankSalary = {
            'name':'Villanelle ' + addExtraChars(9999999),
            'salary':'',
            'age':'28'
        }

        this.zeroSalary = {
            'name':'Villanelle ' + addExtraChars(9999999),
            'salary':'0',
            'age':'28'
        }

        this.negativeSalary = {
            'name':'Villanelle ' + addExtraChars(9999999),
            'salary':'-1',
            'age':'28'
        }

        this.maxSalary = {
            'name':'Villanelle ' + addExtraChars(9999999),
            'salary':'2147483648',
            'age':'28'          
        }

        this.negativeSalary = {
            'name':'Villanelle ' + addExtraChars(9999999),
            'salary':'-1',
            'age':'28'
        }

        this.noAge = {
            'name':'Carolyn Martens ' + addExtraChars(9999999),
            'salary':'280000'
        }

        this.blankAge = {
            'name':'Carolyn Martens ' + addExtraChars(9999999),
            'salary':'100000',
            'age':''
        }

        this.zeroAge = {
            'name':'Carolyn Martens ' + addExtraChars(9999999),
            'salary':'200300',
            'age':'0'
        }

        this.negativeAge = {
            'name':'Carolyn Martens ' + addExtraChars(9999999),
            'salary':'100200',
            'age':'-1'
        }

        this.maxAge = {
            'name':'Carolyn Martens ' + addExtraChars(9999999),
            'salary':'150000',
            'age':'2147483648'          
        }

        this.withId = {
            'name':'Konstantin ' + addExtraChars(9999999),
            'salary':'200002',
            'age':'55',
            'id':addExtraChars(9999999)
        }

        this.updateEmployee = {
            'name':'Phoebe Waller-Bridge',
            'salary':'500000',
            'age':'35',
            'id':addExtraChars(9999999)
        }

        this.updateNameOnly = {
            'name':'Kenny Stowton',
            'salary':'0',
            'age':'0'
        }

        this.updateSalaryOnly = {
            'salary':'500000'
        }

        this.updateAgeOnly = {
            'age':'35'
        }
    
    }

    return EmployeesTestDataList;
})();

module.exports = EmployeesTestDataList;