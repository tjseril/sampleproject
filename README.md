# API TESTS #

API Documentation - http://dummy.restapiexample.com

This repository contains a suite of tests for the above API using NodeJS. The tests use **mocha** to structure the tests, **chai** for assertions and **chakram** to perform the end-to-end tests on the REST API endpoints.

## Usage Instructions ##

This project requires NodeJS with npm (latest versions 13.5.0 and 6.13.4, respectively). To ensure that both are installed, run `node -v && npm -v` in the command line. To install npm, run `npm install` command from the working directory.

The `package.json` outlines the packages used. 

### To Run The Project ###

1. Open up a terminal window and navigate to your local working directory
2. Ensure that the `node_modules` folder has been created using `package.json` (this is usually created after npm installation)
3. To execute tests, run the following commands (in this logical sequence):
    1. mocha Tests/CreateEmployee.js
    2. mocha Tests/GetAllEmployees.js
    3. mocha Tests/GetSingleEmployee.js
    4. mocha Tests/UpdateEmployee.js
    5. mocha Tests/DeleteEmployee.js
4. To execute the tests and log results using **mocha-junit-reporter** (logs are saved in the `.\Output` folder), run the following instead:
    1. npm run createEmployee
    2. npm run getAllEmployees
    3. npm run getSingleEmployee
    4. npm run updateEmployee
    5. npm run deleteEmployee

## Things To Note ##

It is assumed that the current behaviour of the REST API is the intended behaviour as per the business requirements (which is not provided for this exercise). 

The automated tests were formulated based on this assumption, even though there are certain scenarios which seems to have been based on vague requirements or just realistically incorrect. 

Here are a few examples of such scenarios:

* All responses have 200 response codes. It should utilise proper HTTP response codes, e.g. 400 for invalid values in fields, 409 for attempts to create duplicate records, etc.
* Providing blank values in name, salary and age in th POST request. There should be validation to decline requests for these scenarios, especially for name (which needs to be mandatory).
* No character validation (accepted characters) - this presents as a problem, especially for security (SQL injections, XSS attacks).
* No field length validations - there's no predefined max lengths for each field. This is required, especially when values are stored in a database table. This fails the tests in GetSingleEmployee.js where the data obtained from the GET request (e.g. maximum int value is stored for salary and age) is compared with the data created by the POST request (i.e. max int value + 1).
* No type validations, e.g. can enter a non-string value in name.
* Can send requests with additional fields which are not part of the schema, and still creates the employee record successfully.
* Can still send the id field in the request body (the value is ignored, however) even though it is automatically generated upon record creation.
* Deleting non-existent IDs return the successful response