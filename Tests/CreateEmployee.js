const chakram = require('chakram');
const chai = require('chai');
const expect = chakram.expect;
const assert = chai.assert;
const fs = require('fs');

var DummyUrlListObject = require('../Variables/DummyUrlListObject.js');
var endpointUrl = new DummyUrlListObject();
var createEmployeeUrl = endpointUrl.baseUrl + endpointUrl.createEmployee;

var EmployeesTestDataListObject = require('../Input/EmployeesTestData.js');
var employeesTestData = new EmployeesTestDataListObject();

var testDataJson = 'Output/createEmployeeTestData.json';
var testDataArray = [];

describe('Create Employee Tests', function(){
    this.timeout(60000);

    it('Test Case CR_EMP_01: Create Employee - Positive Test', function(){
        var response = chakram.post(createEmployeeUrl, employeesTestData.newEmployee);
        return chakram.waitFor([
            expect(response).to.have.json(function(resbody){
                testDataArray.push(resbody),
                expect(resbody.name).to.equal(employeesTestData.newEmployee.name),
                expect(resbody.salary).to.equal(employeesTestData.newEmployee.salary),
                expect(resbody.age).to.equal(employeesTestData.newEmployee.age),
                assert.exists(resbody.id)
            })
        ])
    });

    it('Test Case CR_EMP_02: Create Employee - Missing Name', function(){
        var response = chakram.post(createEmployeeUrl, employeesTestData.noName);
        return chakram.waitFor([
            expect(response).to.have.json(function(resbody){
                expect(resbody).to.equal(employeesTestData.errorMsgNoName.errorMsg)
            })
        ])
    });

    it('Test Case CR_EMP_03: Create Employee - Blank Value in Name', function(){
        var response = chakram.post(createEmployeeUrl, employeesTestData.blankName);
        return chakram.waitFor([
            expect(response).to.have.json(function(resbody){
                expect(resbody).to.equal(employeesTestData.errorMsgBlankName.errorMsg)
            })
        ])
    });

    it('Test Case CR_EMP_04: Create Employee - Name with Special Characters', function(){
        var response = chakram.post(createEmployeeUrl, employeesTestData.specialCharsName);
        return chakram.waitFor([
            expect(response).to.have.json(function(resbody){
                testDataArray.push(resbody),
                expect(resbody.name).to.equal(employeesTestData.specialCharsName.name),
                expect(resbody.salary).to.equal(employeesTestData.specialCharsName.salary),
                expect(resbody.age).to.equal(employeesTestData.specialCharsName.age),
                assert.exists(resbody.id)
            })
        ])
    });

    it('Test Case CR_EMP_05: Create Employee - Name with Max+1 Character Limit', function(){
        var response = chakram.post(createEmployeeUrl, employeesTestData.maxCharsName);
        return chakram.waitFor([
            expect(response).to.have.json(function(resbody){
                testDataArray.push(resbody),
                expect(resbody.name).to.equal(employeesTestData.maxCharsName.name),
                expect(resbody.salary).to.equal(employeesTestData.maxCharsName.salary),
                expect(resbody.age).to.equal(employeesTestData.maxCharsName.age),
                assert.exists(resbody.id)
            })
        ])
    });

    it('Test Case CR_EMP_06: Create Employee - Name with Non-String Format', function(){
        var response = chakram.post(createEmployeeUrl, employeesTestData.nonStringName);
        return chakram.waitFor([
            expect(response).to.have.json(function(resbody){
                testDataArray.push(resbody),
                expect(resbody.name).to.equal(employeesTestData.nonStringName.name),
                expect(resbody.salary).to.equal(employeesTestData.nonStringName.salary),
                expect(resbody.age).to.equal(employeesTestData.nonStringName.age),
                assert.exists(resbody.id)
            })
        ])
    });

    it('Test Case CR_EMP_07: Create Employee - Duplicate Name', function(){
        var response = chakram.post(createEmployeeUrl, employeesTestData.duplicateName);
        return chakram.waitFor([
            expect(response).to.have.json(function(resbody){
                expect(resbody).to.equal(employeesTestData.errorMsgDuplicateName.errorMsg)
            })
        ])
    });

    it('Test Case CR_EMP_08: Create Employee - Missing Salary', function(){
        var response = chakram.post(createEmployeeUrl, employeesTestData.noSalary);
        return chakram.waitFor([
            expect(response).to.have.json(function(resbody){
                expect(resbody).to.equal(employeesTestData.errorMsgNoSalary.errorMsg)
            })
        ])
    });
    
    it('Test Case CR_EMP_09: Create Employee - Blank Salary', function(){
        var response = chakram.post(createEmployeeUrl, employeesTestData.blankSalary);
        return chakram.waitFor([
            expect(response).to.have.json(function(resbody){
                testDataArray.push(resbody),
                expect(resbody.name).to.equal(employeesTestData.blankSalary.name),
                expect(resbody.salary).to.equal(employeesTestData.blankSalary.salary),
                expect(resbody.age).to.equal(employeesTestData.blankSalary.age),
                assert.exists(resbody.id)
            })
        ])       
    });

    it('Test Case CR_EMP_10: Create Employee - Zero Salary', function(){
        var response = chakram.post(createEmployeeUrl, employeesTestData.zeroSalary);
        return chakram.waitFor([
            expect(response).to.have.json(function(resbody){
                testDataArray.push(resbody),
                expect(resbody.name).to.equal(employeesTestData.zeroSalary.name),
                expect(resbody.salary).to.equal(employeesTestData.zeroSalary.salary),
                expect(resbody.age).to.equal(employeesTestData.zeroSalary.age),
                assert.exists(resbody.id)
            })
        ])       
    })

    it('Test Case CR_EMP_11: Create Employee - Max Allowed Salary Value', function(){
        var response = chakram.post(createEmployeeUrl, employeesTestData.maxSalary);
        return chakram.waitFor([
            expect(response).to.have.json(function(resbody){
                testDataArray.push(resbody),
                expect(resbody.name).to.equal(employeesTestData.maxSalary.name),
                expect(resbody.salary).to.equal(employeesTestData.maxSalary.salary),
                expect(resbody.age).to.equal(employeesTestData.maxSalary.age),
                assert.exists(resbody.id)
            })
        ])       
    })

    it('Test Case CR_EMP_12: Create Employee - Negative Salary', function(){
        var response = chakram.post(createEmployeeUrl, employeesTestData.negativeSalary);
        return chakram.waitFor([
            expect(response).to.have.json(function(resbody){
                testDataArray.push(resbody),
                expect(resbody.name).to.equal(employeesTestData.negativeSalary.name),
                expect(resbody.salary).to.equal(employeesTestData.negativeSalary.salary),
                expect(resbody.age).to.equal(employeesTestData.negativeSalary.age),
                assert.exists(resbody.id)
            })
        ])       
    })

    it('Test Case CR_EMP_13: Create Employee - Missing Age', function(){
        var response = chakram.post(createEmployeeUrl, employeesTestData.noAge);
        return chakram.waitFor([
            expect(response).to.have.json(function(resbody){
                expect(resbody).to.equal(employeesTestData.errorMsgNoAge.errorMsg)
            })
        ])
    });
    
    it('Test Case CR_EMP_14: Create Employee - Blank Age', function(){
        var response = chakram.post(createEmployeeUrl, employeesTestData.blankAge);
        return chakram.waitFor([
            expect(response).to.have.json(function(resbody){
                testDataArray.push(resbody),
                expect(resbody.name).to.equal(employeesTestData.blankAge.name),
                expect(resbody.salary).to.equal(employeesTestData.blankAge.salary),
                expect(resbody.age).to.equal(employeesTestData.blankAge.age),
                assert.exists(resbody.id)
            })
        ])       
    });

    it('Test Case CR_EMP_15: Create Employee - Zero Age', function(){
        var response = chakram.post(createEmployeeUrl, employeesTestData.zeroAge);
        return chakram.waitFor([
            expect(response).to.have.json(function(resbody){
                testDataArray.push(resbody),
                expect(resbody.name).to.equal(employeesTestData.zeroAge.name),
                expect(resbody.salary).to.equal(employeesTestData.zeroAge.salary),
                expect(resbody.age).to.equal(employeesTestData.zeroAge.age),
                assert.exists(resbody.id)
            })
        ])       
    })

    it('Test Case CR_EMP_16: Create Employee - Max Allowed Age Value', function(){
        var response = chakram.post(createEmployeeUrl, employeesTestData.maxAge);
        return chakram.waitFor([
            expect(response).to.have.json(function(resbody){
                testDataArray.push(resbody),
                expect(resbody.name).to.equal(employeesTestData.maxAge.name),
                expect(resbody.salary).to.equal(employeesTestData.maxAge.salary),
                expect(resbody.age).to.equal(employeesTestData.maxAge.age),
                assert.exists(resbody.id)
            })
        ])       
    })

    it('Test Case CR_EMP_17: Create Employee - Negative Age', function(){
        var response = chakram.post(createEmployeeUrl, employeesTestData.negativeAge);
        return chakram.waitFor([
            expect(response).to.have.json(function(resbody){
                testDataArray.push(resbody),
                expect(resbody.name).to.equal(employeesTestData.negativeAge.name),
                expect(resbody.salary).to.equal(employeesTestData.negativeAge.salary),
                expect(resbody.age).to.equal(employeesTestData.negativeAge.age),
                assert.exists(resbody.id)
            })
        ])       
    })

    it('Test Case CR_EMP_18: Create Employee - With ID Provided', function(){
        var response = chakram.post(createEmployeeUrl, employeesTestData.withId);
        return chakram.waitFor([
            expect(response).to.have.json(function(resbody){
                testDataArray.push(resbody),
                expect(resbody.name).to.equal(employeesTestData.withId.name),
                expect(resbody.salary).to.equal(employeesTestData.withId.salary),
                expect(resbody.age).to.equal(employeesTestData.withId.age),
                assert.exists(resbody.id)
            })
        ])       
    })

    it('Save All Created Test Data', function(){
            fs.writeFileSync(testDataJson, JSON.stringify(testDataArray));
    })

})