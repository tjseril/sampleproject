const chakram = require('chakram');
const chai = require('chai');
const expect = chakram.expect;
const assert = chai.assert;
const fs = require('fs');

var DummyUrlListObject = require('../Variables/DummyUrlListObject.js');
var endpointUrl = new DummyUrlListObject();
var deleteEmployeeUrl = endpointUrl.baseUrl + endpointUrl.deleteEmployee;

var EmployeesTestDataListObject = require('../Input/EmployeesTestData.js');
var employeesTestData = new EmployeesTestDataListObject();

var testDataJson = fs.readFileSync('./Output/createEmployeeTestData.json');
var createEmployeeTestData = JSON.parse(testDataJson);

describe('Delete Employee Tests', function(){
    this.timeout(60000);

    for(let count = 0; count < createEmployeeTestData.length; count++){
        it('Test Case DEL_EMP_01.' + count + ': Delete Employee - Positive Test', function(){
            var response = chakram.delete(deleteEmployeeUrl + createEmployeeTestData[count].id);
            return chakram.waitFor([
                expect(response).to.have.json(function(resbody){
                    assert.exists(resbody.success)
                })
            ])   
        })
    }

    it('Test Case DEL_EMP_02: Delete Non-Existent Employee', function(){
        var response = chakram.delete(deleteEmployeeUrl + 'xoxo');
        return chakram.waitFor([
            expect(response).to.have.json(function(resbody){
                assert.exists(resbody.success)
            })
        ])   
    })

})