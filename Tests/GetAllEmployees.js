const chakram = require('chakram');
const chai = require('chai');
const expect = chakram.expect;
const assert = chai.assert;

var DummyUrlListObject = require('../Variables/DummyUrlListObject.js');
var endpointUrl = new DummyUrlListObject();
var getAllEmployeesUrl = endpointUrl.baseUrl + endpointUrl.getAllEmployees;

describe('Get All Employees Tests', function(){
    this.timeout(60000);

    it('Test Case 1: Get All Employees - Positive Test', function(){
        var response = chakram.get(getAllEmployeesUrl);
        return chakram.waitFor([
            expect(response).to.have.status(200)
        ])
    })

    it('Test Case 2: Using Get All Employees endpoint to return single record', function(){
        var response = chakram.get(getAllEmployeesUrl + '/1');
        return chakram.waitFor([
            expect(response).to.have.status(404)
        ])
    })

})