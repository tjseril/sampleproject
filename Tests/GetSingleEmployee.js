const chakram = require('chakram');
const chai = require('chai');
const expect = chakram.expect;
const assert = chai.assert;
const fs = require('fs');

var DummyUrlListObject = require('../Variables/DummyUrlListObject.js');
var endpointUrl = new DummyUrlListObject();
var getSingleEmployeeUrl = endpointUrl.baseUrl + endpointUrl.getSingleEmployee;

var EmployeesTestDataListObject = require('../Input/EmployeesTestData.js');
var employeesTestData = new EmployeesTestDataListObject();

var testDataJson = fs.readFileSync('./Output/createEmployeeTestData.json');
var createEmployeeTestData = JSON.parse(testDataJson);

describe('Get Single Employee Tests', function(){
    this.timeout(60000);

    for(let count = 0; count < createEmployeeTestData.length; count++){
        it('Test Case GET_SING_EMP_01.' + count + ': Get Single Employee - Positive Test', function(){
            var response = chakram.get(getSingleEmployeeUrl + createEmployeeTestData[count].id);
            return chakram.waitFor([
                expect(response).to.have.json(function(resbody){
                    expect(resbody.employee_name).to.equal(createEmployeeTestData[count].name),
                    expect(resbody.employee_salary).to.equal(createEmployeeTestData[count].salary),
                    expect(resbody.employee_age).to.equal(createEmployeeTestData[count].age),
                    assert.exists(resbody.profile_image)
                })
            ])   
        })
    }

    it('Test Case GET_SING_EMP_02: Get Single Employee with no ID', function(){
        var response = chakram.get(getSingleEmployeeUrl);
        return chakram.waitFor([
            expect(response).to.have.status(404)
        ])
    })

    it('Test Case GET_SING_EMP_03: Get Single Employee with non-existent ID', function(){
        var response = chakram.get(getSingleEmployeeUrl + 'abc123');
        return chakram.waitFor([
            expect(response).to.have.json(function(resbody){
                expect(resbody).to.equal(employeesTestData.errorMsgNonExistentId.errorMsg)
            })
        ])
    })

})