const chakram = require('chakram');
const chai = require('chai');
const expect = chakram.expect;
const assert = chai.assert;
const fs = require('fs');

var DummyUrlListObject = require('../Variables/DummyUrlListObject.js');
var endpointUrl = new DummyUrlListObject();
var getSingleEmployeeUrl = endpointUrl.baseUrl + endpointUrl.getSingleEmployee;
var updateEmployeeUrl = endpointUrl.baseUrl + endpointUrl.updateEmployee;

var EmployeesTestDataListObject = require('../Input/EmployeesTestData.js');
var employeesTestData = new EmployeesTestDataListObject();

var testDataJson = fs.readFileSync('./Output/createEmployeeTestData.json');
var createEmployeeTestData = JSON.parse(testDataJson);

describe('Update Employee Tests', function(){
    this.timeout(60000);

    it('Test Case UPD_EMP_01a: Update Employee (All Fields) - Positive Test', function(){
        var response = chakram.put(updateEmployeeUrl + createEmployeeTestData[0].id,employeesTestData.updateEmployee);
        return chakram.waitFor([
            expect(response).to.have.json(function(resbody){
                expect(resbody.name).to.equal(employeesTestData.updateEmployee.name),
                expect(resbody.salary).to.equal(employeesTestData.updateEmployee.salary),
                expect(resbody.age).to.equal(employeesTestData.updateEmployee.age)
            })
        ])   
    })
    
    it('Test Case UPD_EMP_01b: Updated Data Verification through GET request', function(){
        var response = chakram.get(getSingleEmployeeUrl + createEmployeeTestData[0].id,);
        return chakram.waitFor([
            expect(response).to.have.json(function(resbody){
                expect(resbody.employee_name).to.equal(employeesTestData.updateEmployee.name),
                expect(resbody.employee_salary).to.equal(employeesTestData.updateEmployee.salary),
                expect(resbody.employee_age).to.equal(employeesTestData.updateEmployee.age),
                assert.exists(resbody.profile_image)
            })
        ])   
    })

    it('Test Case UPD_EMP_02a: Update Name Only - Positive Test', function(){
        var response = chakram.put(updateEmployeeUrl + createEmployeeTestData[1].id,employeesTestData.updateNameOnly);
        return chakram.waitFor([
            expect(response).to.have.json(function(resbody){
                expect(resbody.name).to.equal(employeesTestData.updateNameonly.name),
                expect(resbody.salary).to.be.null,
                expect(resbody.age).to.be.null
            })
        ])   
    })
    
    it('Test Case UPD_EMP_02b: Updated Data Verification through GET request', function(){
        var response = chakram.get(getSingleEmployeeUrl + createEmployeeTestData[1].id,);
        return chakram.waitFor([
            expect(response).to.have.json(function(resbody){
                expect(resbody.employee_name).to.equal(employeesTestData.updateNameOnly.name),
                expect(resbody.employee_salary).to.equal("0"),
                expect(resbody.employee_age).to.equal("0"),
                assert.exists(resbody.profile_image)
            })
        ])   
    })

    it('Test Case UPD_EMP_03: Update Salary Only - Negative Test', function(){
        var response = chakram.put(updateEmployeeUrl + createEmployeeTestData[2].id,employeesTestData.updateSalaryOnly);
        return chakram.waitFor([
            expect(response).to.have.json(function(resbody){
                expect(resbody).to.equal(employeesTestData.errorMsgBlankName.errorMsg)
            })
        ])   
    })

    it('Test Case UPD_EMP_04: Update Age Only - Negative Test', function(){
        var response = chakram.put(updateEmployeeUrl + createEmployeeTestData[2].id,employeesTestData.updateAgeOnly);
        return chakram.waitFor([
            expect(response).to.have.json(function(resbody){
                expect(resbody).to.equal(employeesTestData.errorMsgBlankName.errorMsg)
            })
        ])   
    })

})