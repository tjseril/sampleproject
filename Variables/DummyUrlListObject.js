var DummyUrlList = (function() {
  function DummyUrlList() {
      
    this.baseUrl = 'http://dummy.restapiexample.com/api/v1/';
    this.getAllEmployees = 'employees';
    this.getSingleEmployee = 'employee/';
    this.createEmployee = 'create';
    this.updateEmployee = 'update/';
    this.deleteEmployee = 'delete/';

  };

  return DummyUrlList;

})();
  
module.exports = DummyUrlList;